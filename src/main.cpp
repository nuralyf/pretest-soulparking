// Import File
#include <Arduino.h>
#include <DHT.h>

// Define PIN
#define PIN_DHT 2
#define DHT_TYPE DHT11
#define PIN_BUZZER 5
#define PIN_LED_HEAT 7
#define PIN_LED_COOL 6

// Setup Sensor
DHT sensor_dht(PIN_DHT, DHT_TYPE);

// Variabel for Pin
enum status {
  COOL,
  HEAT
};

const uint8_t _count_avg = 16, limit_temp = 35;
uint8_t _next_buff;
float get_temp_dht11, _buff_temp[_count_avg], _run_avg_temp, _final_temp;


// Function for LED
void ledColor(uint8_t status) {
  if(status == COOL) {
    digitalWrite(PIN_LED_COOL, HIGH);
    digitalWrite(PIN_LED_HEAT, LOW);
  } else if(status == HEAT) {
    digitalWrite(PIN_LED_COOL, LOW);
    digitalWrite(PIN_LED_HEAT, HIGH);
  }
}

// Function for Buzzer Tone
void buzzerTone(uint8_t status) {
  if (status == HEAT) {
    tone(PIN_BUZZER, 15);
  } else if(status == COOL) {
    noTone(PIN_BUZZER);
  }
}

// Function for Get the temperature
float tempDHT() {
  get_temp_dht11 = sensor_dht.readTemperature();
  _buff_temp[_next_buff++] = get_temp_dht11;

  if(_next_buff >= _count_avg) {
    _next_buff = 0;
  }  

  _run_avg_temp = 0;

  for(uint8_t loop = 0; loop < _count_avg; ++loop) {
    _run_avg_temp += _buff_temp[loop];
  }

  _final_temp = (_run_avg_temp /= _count_avg);

  return _final_temp;
}

// Function for Serial Monitor System Status
void serialStatus(uint8_t status) {
  String text_temp = "Temp: ";
  String text_status = "Condition: ";
  float temp = tempDHT();
  String tmp = String(temp);

  if(status == COOL) {
    Serial.println(text_temp + tmp + ", " + text_status + "Cool");
  } else if(status == HEAT) {
    Serial.println(text_temp + tmp + ", " + text_status + "Heat");
  }
} 

// Setup the Arduino
void setup() {
  // Serial setup
  Serial.begin(9600);
  // Setup DHT
  sensor_dht.begin();
  // Setup LED
  pinMode(PIN_LED_COOL, OUTPUT);
  pinMode(PIN_LED_HEAT, OUTPUT);
  // Seetup Buzzer
  pinMode(PIN_BUZZER, OUTPUT);
}

// Main Loop Program
void loop() {
  if(tempDHT() >= limit_temp) {
    serialStatus(HEAT);
    ledColor(HEAT);
    buzzerTone(HEAT);
  } else if(tempDHT() < limit_temp) {
    serialStatus(COOL);
    ledColor(COOL);
    buzzerTone(COOL);
  }
}