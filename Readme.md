﻿# Temperature Detection System
> Pretest task for Soul Parking. Room temperature detection system using Arduino Nano with Sensor as input, also LED and Sound as output.

### Features
- Detect temperature with DHT11
- Console log via Serial monitor
- LED Variation (Blue for cool, Red for heat)
- Buzzer output when temp heat up to 35°C

### Component
1. Arduino Nano
2. DHT11
3. RGB LED (SMD)
4. Piezo Buzzer
### Pin & Wiring
|Component|Pin||
|--|--|--|
|DHT11|D2|`Input`|
|RGB LED (Red)|D7|`Output`|
|RGB LED (Blue)|D6|`Output`|
|Piezo Buzzer|D5|`Output`|
```mermaid
graph LR
B((DHT11)) -- Input --> A[Arduino Nano]
A[Arduino Nano] -- Output --> C(LED RGB) 
A[Arduino Nano] -- Output --> D(Piezo Buzzer) 
```

#### PCB & Schematic
![Schematic](./pcb/docs/schematic.png)
![PCB](./pcb/docs/pcb.png)


#### Result
![Result 1](./pcb/docs/3805.jpg)
![Result 2](./pcb/docs/3806.jpg)
![Result 3](./pcb/docs/3807.jpg)